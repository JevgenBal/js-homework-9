"use strict"

function displayList(arr, parent = document.body) {
  let ul = document.createElement("ul");
  parent.appendChild(ul);

  arr.forEach((element) => {
    let li = document.createElement("li");
    li.textContent = element;
    ul.appendChild(li);
  });
}

let arrCity = ["Hello", "world", "Kiev", ["Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv"];
displayList(arrCity);

let arrNumbers = ["1", "2", "3", "sea", "user", 23];
displayList(arrNumbers);